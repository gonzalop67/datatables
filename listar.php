<?php

	include ("conexion.php");

	$query = "SELECT * FROM usuario WHERE estado = 1 ORDER BY idusuario desc;";
	$resultado = mysqli_query($conexion, $query);

	if(!$resultado){
		die("Error");
	}else{
		$array["data"] = []; //devuelve un arreglo vacío por si no hay registros en la base de datos.
		while($data=mysqli_fetch_assoc($resultado)){
			$arreglo["data"][]=array_map("utf8_encode",$data);
		}
		echo json_encode($arreglo);
	}

	mysqli_free_result($resultado);
	mysqli_close($conexion);

?>